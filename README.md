# NYC Open Data Analysis: Investigating Trends of NYC Residents



### To ShowCase:
* #### Top Ten Complaints
* #### Top ten most Populous Zipcodes
* #### Quantity of Top Ten Complaints per Burough/zipcode
* #### Index, by borough, based on the number of complaints per population


----------------------------------------------
## To Start up Environment (in hindsight should have exposed port 8888)

* **Docker pull joshuadeiner01/city_data_env:version_three**

* **docker run -p 8889:8881 joshuadeiner01/city_data_env:version_three**

* **Open up Jupyter in browser with terminal instructions. (Ie, use URL  http://127.0.0.1:8889 with given token)**

* **Open up Notebooks Directory**

    * **Open up boroughs.ipynb and run all cells**

    * **Open up complaint_index.ipynb  and run all cells # may need to run import lines twice**

    * **Open up zipcodes.ipynb and run all cells  # may need to run import lines twice**


----------------------------------------------
#### Data Sources

* Population                : [link](https://blog.splitwise.com/2013/09/18/the-2010-us-census-population-by-zip-code-totally-free/) 
* General Census Information: [link](https://data.cityofnewyork.us/Social-Services/311-Service-Requests-from-2010-to-Present/erm2-nwe9)

 